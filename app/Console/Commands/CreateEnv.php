<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CreateEnv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'youpks:install';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs Youpks';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(base_path('.env'))) {
            $this->info('.env file already exists!');

            return;
        }

        $options['APP_NAME'] = $this->ask('Wat is de naam van het project?');
        $options['APP_ENV'] = $this->choice(
            'Wat is de omgeving van uw website?',
            ['local', 'development', 'production'],
            0,
        );
        $options['APP_DEBUG'] = $this->choice('Debugmodus aanzetten?', ['false', 'true'], 1);

        $options['APP_LOG_LEVEL'] = $options['APP_ENV'] === 'production' ? 'info' : 'debug';

        $options['APP_URL'] = $this->ask('Wat is de URL van uw (lokale) website?', 'https://localhost:4430');
        $options['DB_DATABASE'] = $this->ask('Wat is de naam van de lokale database?', 'youpks');
        $options['DB_USERNAME'] = $this->ask('Wat is de gebruikersnaam voor de database?', 'youpks');
        $options['DB_PASSWORD'] = $this->ask('Wat is het wachtwoord voor de database?', 'secret');
        $options['DB_HOST'] = $this->ask('Wat is de host van de database?', 'db');
        $options['DB_PORT'] = (int) $this->ask('Wat is de poort van de database?', 3306);

        $options['CACHE_DRIVER'] = $this->choice('Welke cachedriver wil je gebruiken?', ['file', 'redis', 'array'], 1);

        if ($options['CACHE_DRIVER'] === 'redis') {
            $options['CACHE_PREFIX'] = $this->ask('Geef een prefix voor de cache:', 'youpks');
        }

        $options['QUEUE_DRIVER'] = $this->choice('Welke queuedriver wil je gebruiken?', ['sync', 'redis'], 1);

        if ($options['QUEUE_DRIVER'] === 'redis') {
            $options['REDIS_HOST'] = $this->ask('Wat is de host van redis?', 'redis');
            $options['QUEUE'] = $this->ask('Geef een naam voor de redis queue', 'youpks');
        } else {
            $options['QUEUE'] = null;
        }

        $options['MAIL_DRIVER'] = $this->choice('Welke e-maildriver wil je gebruiken?', ['smtp', 'mail'], 0);
        $options['MAIL_HOST'] = $this->ask('Wat is de e-mailhost?', 'mailtrap.io');
        $options['MAIL_PORT'] = (int) $this->ask('Wat is de poort van de e-mailhost?', 2525);
        $options['MAIL_USERNAME'] = $this->ask(
            'Wat is de gebruikersnaam voor de e-mailhost?',
            getenv('HOMESTEAD_MAIL_USERNAME') ?? null,
        );
        $options['MAIL_PASSWORD'] = $this->ask(
            'Wat is het wachtwoord voor de e-mailhost?',
            getenv('HOMESTEAD_MAIL_PASSWORD') ?? null,
        );
        $options['MAIL_FROM_ADDRESS'] = $this->ask('Wat is het e-mailadres van de afzender?', 'info@youpks.nl');
        $options['MAIL_FROM_NAME'] = $this->ask('Wat is de naam van de afzender?', 'CMeleon');

        copy(base_path('.env.example'), base_path('.env'));

        foreach ($options as $optionName => $optionValue) {
            if ($this->writeSetting($optionName, $optionValue)) {
                $this->info('Added setting for '.$optionName);
            } else {
                $this->error('Error adding setting for '.$optionName);
            }
        }
    }

    /**
     * Replace a placeholder with a given value.
     *
     * @param $name string Placeholder
     * @param $value string Setting value
     * @return bool
     */
    private function writeSetting($name, $value)
    {
        $pathToEnvFile = base_path('.env');
        $contents = File::get($pathToEnvFile);

        $value = preg_match('/\s/', $value) ? '"'.$value.'"' : $value;
        $contents = preg_replace('/'.preg_quote($name, '/').'=.*?$/m', "$name=$value", $contents);

        return File::put($pathToEnvFile, $contents) !== false;
    }
}
